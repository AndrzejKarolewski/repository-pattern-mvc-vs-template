﻿using MVC_RepositoryPattern_Template.Models;
using MVC_RepositoryPattern_Template.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_RepositoryPattern_Template.Controllers
{
    public class HomeController : Controller
    {
        private IProjectService projectService;

        public HomeController(IProjectService projectService)
        {
            this.projectService = projectService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var project = new Project { Name = $"New Project {Guid.NewGuid()}" };
            projectService.AddProject(project);


            ViewBag.Message = $"Created Project : {project.Name}";            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }


    }
}