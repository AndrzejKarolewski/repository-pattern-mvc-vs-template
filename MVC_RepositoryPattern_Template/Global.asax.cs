﻿using Ninject.Web.Common.WebHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;
using MVC_RepositoryPattern_Template.Models.Repositories;
using Ninject.Web.Common;
using MVC_RepositoryPattern_Template.Models.Repositories.Interfaces;
using MVC_RepositoryPattern_Template.Services.Interfaces;
using MVC_RepositoryPattern_Template.Services;

namespace MVC_RepositoryPattern_Template
{
    public class MvcApplication : NinjectHttpApplication
    {
        protected override void OnApplicationStarted()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            //Here register injections in certain scopes
            kernel.Bind<IRepository>().To<Repository>().InRequestScope();
            kernel.Bind<IProjectService>().To<ProjectService>().InRequestScope();

            return kernel;
        }
    }
}
