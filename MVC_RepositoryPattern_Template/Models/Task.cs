﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_RepositoryPattern_Template.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}