﻿using MVC_RepositoryPattern_Template.Models.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MVC_RepositoryPattern_Template.Models.Repositories
{
    public class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {
        public ProjectRepository(ProjectContext context) : base(context)
        {
        }

        public ProjectContext ProjectContext { get { return Context as ProjectContext; } }


        public IEnumerable<Project> GetFinishedProjects()
        {
            throw new NotImplementedException();
        }
    }
} 