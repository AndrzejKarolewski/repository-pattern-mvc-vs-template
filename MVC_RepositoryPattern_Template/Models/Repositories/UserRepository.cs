﻿using MVC_RepositoryPattern_Template.Models.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVC_RepositoryPattern_Template.Models.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(ProjectContext context) : base(context)
        {
        }

        public ProjectContext ProjectContext { get { return Context as ProjectContext; } } 


        public IEnumerable<User> GetNewlyRegisteredUsers()
        {
            return ProjectContext.Users.Where(x => x.Id > 100).ToList();
        }
    }
}