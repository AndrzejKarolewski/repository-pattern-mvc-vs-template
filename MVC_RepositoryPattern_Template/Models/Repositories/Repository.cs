﻿using MVC_RepositoryPattern_Template.Models.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_RepositoryPattern_Template.Models.Repositories
{
    public class Repository : IRepository
    {
        private ProjectContext context;

        public Repository(ProjectContext context)
        {
            this.context = context;
            Projects = new ProjectRepository(context);
            Users = new UserRepository(context);
        }

        public IProjectRepository Projects { get; private set; }       
        public IUserRepository Users { get; private set; }

        public int Commit()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}