﻿using System.Data.Entity;

namespace MVC_RepositoryPattern_Template.Models.Repositories
{
    public class ProjectContext : DbContext
    {
        public ProjectContext() : base("name=ProjectContext")
        {
                
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
    }
}