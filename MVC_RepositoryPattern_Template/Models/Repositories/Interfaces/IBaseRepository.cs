﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC_RepositoryPattern_Template.Models.Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {       
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);

        T GetById(int id);
        IEnumerable<T> GetAll();           
    }
}
