﻿using System.Collections.Generic;

namespace MVC_RepositoryPattern_Template.Models.Repositories.Interfaces
{
    public interface IProjectRepository : IBaseRepository<Project>
    {
        IEnumerable<Project> GetFinishedProjects();
    }
}