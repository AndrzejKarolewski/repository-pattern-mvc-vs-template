﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC_RepositoryPattern_Template.Models.Repositories.Interfaces
{
    public interface IRepository : IUnitOfWork
    {
        IUserRepository Users { get; }
        IProjectRepository Projects { get; }
    }
}
