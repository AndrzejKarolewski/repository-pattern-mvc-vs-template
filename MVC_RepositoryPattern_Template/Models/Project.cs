﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_RepositoryPattern_Template.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<User> Members { get; set; }
        public virtual List<Task> Tasks { get; set; }
    }
}