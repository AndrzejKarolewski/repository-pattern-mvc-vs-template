﻿using MVC_RepositoryPattern_Template.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC_RepositoryPattern_Template.Services.Interfaces
{
    public interface IProjectService
    {
        void AddProject(Project project);
    }
}
