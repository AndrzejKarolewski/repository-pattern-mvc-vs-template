﻿using MVC_RepositoryPattern_Template.Models;
using MVC_RepositoryPattern_Template.Models.Repositories.Interfaces;
using MVC_RepositoryPattern_Template.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_RepositoryPattern_Template.Services
{
    public class ProjectService : IProjectService
    {
        private IRepository repository;

        public ProjectService(IRepository repository)
        {
            this.repository = repository;
        }

        public void AddProject(Project project)
        {
            repository.Projects.Add(project);
            repository.Commit();
        }       
    }
}